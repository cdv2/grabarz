using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DeadInfoUI : MonoBehaviour
{
    [SerializeField]
    public DeadInfoScriptableObject _deadInfo;
    [SerializeField]
    private TextMeshProUGUI _name;
    [SerializeField]
    private TextMeshProUGUI _surname;
    [SerializeField]
    private TextMeshProUGUI _birthDate;
    [SerializeField]
    private TextMeshProUGUI _deathDate;
    [SerializeField]
    private TextMeshProUGUI _deathCause;
    [SerializeField]
    private TextMeshProUGUI _story;
    [SerializeField]
    private Image _portrait;

    private void Start()
    {
        _name.text = _deadInfo.name;
        _surname.text = _deadInfo.surname;
        _birthDate.text = _deadInfo.bornYear;
        _deathDate.text = _deadInfo.deathYear;
        _deathCause.text = _deadInfo.deathCause;
        _portrait.sprite = _deadInfo.portrait;
        _story.text = _deadInfo.story;
    }
}
