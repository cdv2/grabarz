using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float _speed = 5.0f;
    [SerializeField]
    private float _mouseSensitivity = 80.0f;
    private CharacterController controller;
    private Vector3 moveDirection;
    private float xRotation = 0.0f;

    [SerializeField]
    private GameObject _terrainTool;

    [SerializeField]
    private Transform _playerCamera;

    
    void Start()
    {
        _terrainTool.SetActive(false);
        controller = GetComponent<CharacterController>();
        
    }

    
    void Update()
    {
        
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");

        Vector3 move = transform.right * moveX + transform.forward * moveZ;
        controller.Move(move * _speed * Time.deltaTime);

        
        float mouseX = Input.GetAxis("Mouse X") * _mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * _mouseSensitivity * Time.deltaTime;

        
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        
        _playerCamera.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);

        
        if (Input.GetMouseButtonDown(0))
        {
            if(_terrainTool.activeSelf)
                _terrainTool.GetComponent<TerrainToolsLive>().PaintTerrainUnderCursor(); 
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            OnOffTerrainTool();
        }
    }

    private void OnOffTerrainTool()
    {
        if (_terrainTool.activeSelf)
        {
            _terrainTool.SetActive(false);
            
        }
        else
        {
            _terrainTool.SetActive(true);
            
        }
    }
}