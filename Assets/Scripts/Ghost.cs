using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    [SerializeField]
    private QuestScriptableObject _quest;
    [SerializeField]
    private Sprite _portrait;
    private bool _isPlayer;


    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Player>() != null)
        {
            _isPlayer = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            _isPlayer = false;
        }
    }

    private void Update()
    {
       if(_isPlayer)
        {
            if(Input.GetKeyDown(KeyCode.E))
            {
                Clock.Instance.OnOffDialogue();

                Clock.Instance.FillQuestInfo(_quest.questText, _portrait);
            }
        }
    }
}

 
