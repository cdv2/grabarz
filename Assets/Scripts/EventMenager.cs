using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventMenager : MonoBehaviour
{
    [Header("Ghosts")]

    [SerializeField]
    private Transform[] _ghostSpawnPositions;
    [SerializeField]
    private GameObject _ghostPrefab;
   

    [SerializeField]
    private GameObject[] _ghostsOnTheScene;
    [SerializeField]
    private GameObject _tombstoneWindow;
    [SerializeField]
    private GameObject _player;




    // Start is called before the first frame update
    private void Start()
    {
        Instantiate();
    }

    private void Update()
    {
        
        GhostsOffOn();

        if(Input.GetKeyDown(KeyCode.T))
        {
            TombstoneWindowOnOff();
        }
        
    }

    private void Instantiate()
    {
        //TOOL

        /*GameObject pickupObjectInstance = Instantiate(_pickUpObjectPrefabs[0], _toolSpawnPosition.position, Quaternion.identity);// instantiate tool on the scene

        PickupObject pickupObjectScript = pickupObjectInstance.GetComponent<PickupObject>();//take refference too its script

        pickupObjectScript.SetPlayer(_player.transform, _playerHand.transform);//Set player and player s hand transform */

        //GHOST

        
        _ghostsOnTheScene = new GameObject[_ghostSpawnPositions.Length];

        
        for (int i = 0; i < _ghostSpawnPositions.Length; i++)
        {
            
            _ghostsOnTheScene[i] = Instantiate(_ghostPrefab, _ghostSpawnPositions[i].position, Quaternion.identity);

            
            _ghostsOnTheScene[i].SetActive(false);
        }


    }

    private bool CheckIfNight()
    {
        if (Clock.Instance != null)
        {
            return Clock.Instance.IsDay;
        }
        else
        {
            throw new System.Exception("Clock instance is null!");
        }
    }

    private void GhostsOffOn()
    {
        if (CheckIfNight())
        {
            for (int i = 0; i < _ghostsOnTheScene.Length; i++)
            {
                _ghostsOnTheScene[i].gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < _ghostsOnTheScene.Length; i++)
            {
                _ghostsOnTheScene[i].gameObject.SetActive(true);
            }
        }
    }

    private void TombstoneWindowOnOff()
    {
        if (_tombstoneWindow.activeSelf)
        {
            _tombstoneWindow.SetActive(false);
            _player.SetActive(true);
        }
        else
        {
            _tombstoneWindow.SetActive(true);
            _player.SetActive(false);
        }
    }
}
