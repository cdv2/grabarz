using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewQuest", menuName = "Custom/ScriptableObject/Quest")]
public class QuestScriptableObject : ScriptableObject
{
    public string questText;
    public int reward;
}
